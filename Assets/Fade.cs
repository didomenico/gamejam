﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fade : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        StartCoroutine(Fades());
	}

    IEnumerator Fades()
    {
        yield return new WaitForSeconds(8);

        GetComponent<Canvas>().enabled = false;
    }


    // Update is called once per frame
    void Update () {
		
	}
}
