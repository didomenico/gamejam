﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerController : MonoBehaviour
{
    private CharacterController cc;

    float speed = 3.5f;

    // Use this for initialization
    void Start ()
	{
	    cc = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update ()
    {            
        Vector3 forwardDirection = Input.GetAxis("Vertical")                                    
	                               * speed
                                   * transform.TransformDirection(Vector3.forward);

        cc.SimpleMove(forwardDirection);

	    Vector3 sideways = Input.GetAxis("Horizontal") *
                            Vector3.up;	          

        transform.Rotate(sideways);
	}
}
